package main

import (
	"dcp-client/internal/config"
	"dcp-client/internal/simulation"
)

func main() {
	cfg := config.LoadClientConfig()
	sim := simulation.NewCustomerSimulation(cfg, simulation.NewDCPHttpClient(cfg))
	sim.Start()
}
