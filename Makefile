build:
	docker build --rm -t dcp-client:1.0.0 -f ./build/Dockerfile .
.PHONY: build

run:
	docker run -it --rm \
		-e SIMULATION.COMPANYCOUNT='1' \
		-e SIMULATION.USERSCOUNTPERCOMPANY='1' \
		-e SIMULATION.USERREQUESTSPERSECOND='1' \
		-e SIMULATION.REQUESTTIMEOUTSECONDS='20' \
		-e ROUTES.BASEURL='http://37.32.25.242:8000' \
		-e ROUTES.LOGINURI='/login' \
		-e ROUTES.REGISTERURI='/users' \
		-e ROUTES.EVENTSURI='/event' \
		dcp-client:1.0.0
