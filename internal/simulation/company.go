package simulation

import (
	"log"
	"strconv"
	"sync"
	"time"
)

type Company struct {
	name     string
	password string
	users    []*User
}

func NewCompany(name, password string, userCount int) *Company {
	users := make([]*User, 0, userCount)
	for i := 0; i < userCount; i++ {
		users = append(users, NewUser(strconv.Itoa(i+1), name))
	}

	return &Company{
		name:     name,
		password: password,
		users:    users,
	}
}

func (c *Company) Start(client DCPClient, sim *CustomerSimulation, rate time.Duration) {
	err := client.Register(c.name, c.password)
	if err != nil {
		log.Println(err)
	}

	token, err := client.Login(c.name, c.password)
	if err != nil {
		log.Println(err)
	}

	wg := sync.WaitGroup{}
	for _, user := range c.users {
		wg.Add(1)
		go func(user *User) {
			user.Start(client, sim, token, rate)
			wg.Done()
		}(user)
	}
	wg.Wait()
}
