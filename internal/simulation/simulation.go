package simulation

import (
	"dcp-client/internal/config"
	"strconv"
	"sync"
	"time"
)

var EventTypes = []string{
	"load_video", "click_about", "pause_video", "play_video", "click_courseware", "close_courseware",
	"stop_video", "seek_video", "problem_get", "problem_check_correct", "problem_check_incorrect",
}

type CustomerSimulation struct {
	client        DCPClient
	lastEventId   int
	lock          sync.Mutex
	companies     []*Company
	userEventRate time.Duration
}

func NewCustomerSimulation(cfg config.ClientConfig, client DCPClient) *CustomerSimulation {
	companies := make([]*Company, 0, cfg.Simulation.CompanyCount)
	for i := 0; i < cfg.Simulation.CompanyCount; i++ {
		companies = append(companies, NewCompany("company"+strconv.Itoa(i+1), "testPassword", cfg.Simulation.UsersCountPerCompany))
	}

	return &CustomerSimulation{
		client:        client,
		lastEventId:   0,
		companies:     companies,
		userEventRate: time.Second * time.Duration(1000.0/float64(cfg.Simulation.UserRequestsPerSecond)) / 1000,
	}
}

func (c *CustomerSimulation) NextEventId() int {
	c.lock.Lock()
	defer c.lock.Unlock()

	c.lastEventId++
	return c.lastEventId
}

func (c *CustomerSimulation) Start() {
	wg := sync.WaitGroup{}
	for _, company := range c.companies {
		wg.Add(1)
		go func(company *Company) {
			company.Start(c.client, c, c.userEventRate)
			wg.Done()
		}(company)
	}

	wg.Wait()
}
