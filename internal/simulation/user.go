package simulation

import (
	"strconv"
	"time"
)

var Courses = []string{"phys", "math", "ap", "compiler", "bigdata"}

type User struct {
	id                  string
	companyId           string
	lastEventType       int
	lastCourse          int
	lastEventSubmission time.Time
}

func NewUser(id, companyId string) *User {
	return &User{
		id:                  id,
		lastEventType:       0,
		lastCourse:          0,
		companyId:           companyId,
		lastEventSubmission: time.Now(),
	}
}

func (u *User) Start(client DCPClient, sim *CustomerSimulation, token string, rate time.Duration) {
	for {
		nextSubmissionTime := u.lastEventSubmission.Add(rate)
		<-time.After(nextSubmissionTime.Sub(time.Now()))

		nextEvent := u.nextEvent(strconv.Itoa(sim.NextEventId()))
		_ = client.SendEvent(token, nextEvent)
	}
}

func (u *User) nextEvent(eventId string) Event {
	u.lastEventType = (u.lastEventType + 1) % len(EventTypes)
	u.lastCourse = (u.lastCourse + 1) % len(Courses)
	u.lastEventSubmission = time.Now()

	return NewEvent(
		eventId,
		EventTypes[u.lastEventType],
		map[string]string{
			"userId":    u.id,
			"sessionId": "session-" + u.id,
			"courseId":  Courses[u.lastCourse],
			"companyId": u.companyId,
		},
		u.lastEventSubmission,
	)
}
