package simulation

import (
	"bytes"
	"dcp-client/internal/config"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type DCPClient interface {
	Register(companyName, password string) error
	Login(companyName, password string) (token string, err error)
	SendEvent(token string, event Event) error
}

type DCPHttpClient struct {
	client *http.Client
	routes config.ApiRoutesConfig
}

var _ DCPClient = (*DCPHttpClient)(nil)

func NewDCPHttpClient(cfg config.ClientConfig) *DCPHttpClient {
	client := &http.Client{Timeout: time.Second * time.Duration(cfg.Simulation.RequestTimeOutSeconds)}
	return &DCPHttpClient{
		client: client,
		routes: cfg.Routes,
	}
}

func (d *DCPHttpClient) Register(companyName, password string) error {
	requestBody := struct {
		Name     string `json:"name"`
		Password string `json:"password"`
	}{Name: companyName, Password: password}

	_, err := d.restCall(d.routes.BaseUrl+d.routes.RegisterUri, http.MethodPost, requestBody, nil)

	return err
}

func (d *DCPHttpClient) Login(companyName, password string) (token string, err error) {
	requestBody := struct {
		Name     string `json:"name"`
		Password string `json:"password"`
	}{Name: companyName, Password: password}

	resp, err := d.restCall(d.routes.BaseUrl+d.routes.LoginUri, http.MethodPost, requestBody, nil)
	if err != nil {
		return "", nil
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	respBody := struct {
		Token string `json:"token"`
	}{Token: ""}
	err = json.Unmarshal(content, &respBody)
	if err != nil {
		return "", err
	}

	return respBody.Token, nil
}

func (d *DCPHttpClient) SendEvent(token string, event Event) error {
	_, err := d.restCall(d.routes.BaseUrl+d.routes.EventsUri, http.MethodPost, event, map[string]string{
		"authorization": token,
	})
	if err != nil {
		return nil
	}

	return nil
}

func (d *DCPHttpClient) restCall(url, method string, body any, headers map[string]string) (*http.Response, error) {
	var bodyReader io.Reader = nil
	if body != nil {
		marshaledBody, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}

		bodyReader = bytes.NewReader(marshaledBody)
	}

	req, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}

	return d.client.Do(req)
}
