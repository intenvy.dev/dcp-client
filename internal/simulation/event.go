package simulation

import (
	"encoding/json"
	"time"
)

type Event struct {
	Id        string `json:"key"`
	EventType string `json:"event"`
	Data      string `json:"json"`
	TimeStamp int64  `json:"ts"`
}

func NewEvent(id, eventType string, data map[string]string, timeStamp time.Time) Event {
	marshaled, _ := json.Marshal(data)
	return Event{
		Id:        id,
		EventType: eventType,
		Data:      string(marshaled),
		TimeStamp: timeStamp.Unix(),
	}
}
