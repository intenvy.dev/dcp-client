package config

type ApiRoutesConfig struct {
	BaseUrl     string
	LoginUri    string
	RegisterUri string
	EventsUri   string
}
