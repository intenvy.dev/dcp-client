package config

type SimulationConfig struct {
	CompanyCount          int
	UsersCountPerCompany  int
	UserRequestsPerSecond int
	RequestTimeOutSeconds int
}
