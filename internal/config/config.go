package config

import (
	"github.com/spf13/viper"
	"log"
	"strings"
)

type ClientConfig struct {
	Simulation SimulationConfig
	Routes     ApiRoutesConfig
}

func LoadClientConfig() ClientConfig {
	var cfg ClientConfig
	v := viper.New()
	setDefaultConfigs(v)
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer("_", "."))
	if err := v.Unmarshal(&cfg); err != nil {
		log.Fatal(err)
	}

	return cfg
}

func setDefaultConfigs(v *viper.Viper) {
	v.SetDefault("simulation.companyCount", 1)
	v.SetDefault("simulation.usersCountPerCompany", 1)
	v.SetDefault("simulation.userRequestsPerSecond", 1)
	v.SetDefault("simulation.requestTimeOutSeconds", 10)
	v.SetDefault("routes.baseUrl", "localhost:8080")
	v.SetDefault("routes.loginUri", "/login")
	v.SetDefault("routes.registerUri", "/register")
	v.SetDefault("routes.eventsUri", "/events")
}
